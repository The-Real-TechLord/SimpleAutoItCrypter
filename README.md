# SimpleAutoItCrypter

Simple AutoIt crypter

## Usage

1. Extract the `SCRIPT` resource as `SCRIPT.bin`
2. Run `encrypt.exe SCRIPT.bin` (optionally change the `encrypt` function)
3. Replace the `SCRIPT` resource with `SCRIPT.bin.enc`
4. Add the `FindResourceExW` import to your executable

Tool for the job: [CFF Explorer](http://www.ntcore.com/exsuite.php).
